const HtmlWebpackPlugin = require('html-webpack-plugin'); //installed via npm
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
module.exports = {
    mode: 'development',
    entry: {
        app: './src/index.js',
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '',
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader'},
                    {
                        loader: 'css-loader',
                        options: { module: true }
                    },
                    { loader: 'sass-loader'}
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({template: './index.html'}),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'static'),
                to: 'static',
                //ignore: ['.*']
            },
            {
                from: path.resolve(__dirname, 'lib'),
                to: 'lib',
                //ignore: ['.*']
            }
        ])
    ],
    optimization: {
        minimize: true,
        splitChunks: {},
        concatenateModules: true,
    },
	devtool: 'source-map',
    resolve: {
        modules: ['node_modules', './lib'],
        extensions: ['.js', '.json', '.jsx', '.css'],
    },
    devServer: {
        port: 8000,
        inline: true,
    }
};
