import BUilder from './js/Builder';
import Container from './js/Container';
import Header from './js/Header';
import WebGLView from './js/WebGLView';
import SideBar from './js/sidebar/SideBar';
import './js/sidebar/SideBar.Import';
import './js/sidebar/SideBar.Geometries';
import './js/sidebar/SideBar.Widgets';
import './js/sidebar/SideBar.Properties';
import './js/sidebar/SideBar.Properties.Settings';
import './js/sidebar/SideBar.Properties.Transforms';
import './js/sidebar/SideBar.Properties.Animations';
import './js/sidebar/SideBar.Properties.Pointers';
import './js/sidebar/SideBar.Scenes';
import './js/sidebar/SideBar.Scenes.Settings';
import './js/sidebar/SideBar.Animation';
import './js/sidebar/SideBar.Animation.Sequence';
import './js/sidebar/SideBar.Animation.Translate';
import './js/sidebar/SideBar.Animation.Scale';
import './js/sidebar/SideBar.Animation.Rotation';
import ControlToolBar from './js/ControlToolBar';
import InteractionToolBar from './js/InteractionToolBar';


const builder = new BUilder();

let element = document.getElementById('builder-container');
let container = new Container.Ui(element);

let header  = new Header(builder);
let leftSideBar = new SideBar(builder, ['Import', 'Geometries', 'Widgets']).setClass('left');
let webGLView = new WebGLView(builder);
// let rightSideBar = new SideBar(builder, ['Properties', 'Scenes']).setClass('right');
let rightSideBar = new SideBar(builder, ['Animation']).setClass('right');
let controlToolBar = new ControlToolBar(builder);
let interactionToolBar = new InteractionToolBar(builder);

container.add(header);
container.add(leftSideBar);
container.add(webGLView);
container.add(controlToolBar);
container.add(interactionToolBar);
container.add(rightSideBar);


builder.events.renderStart.dispatch();

window.addEventListener( 'resize', onWindowResize, false );

function onWindowResize( event ) {
	builder.events.windowResize.dispatch();
}
