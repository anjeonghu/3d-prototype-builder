/**
 * Created by front on 2019-01-30.
 */
import * as THREE from 'three.module'
import { TransformControls } from 'TransformControls.js'
import { OrbitControls } from 'OrbitControls'
import Container from './Container';
import AddObjectCommand from './command/AddObjectCommand';
import Add3DPointerCommand from "./command/Add3DPointerCommand";
import RemoveObjectCommand from './command/RemoveObjectCommand';
import Remove3DPointerCommand from './command/Remove3DPointerCommand';

const WebGLView = function(builder) {
    let events = builder.events;
    let light = builder.light;
    let renderer = builder.renderer;
    let camera = builder.camera;
    let objects = builder.objects;
    let animationId = null;
    let container = new Container.WebGLView();
    let scene = builder.scene;
    let scenes = builder.scenes;
    let boxhelper = new THREE.BoxHelper();
    let clock = new THREE.Clock();
    let mouseHelper = null;
    let mouseHelperLine = null;

    renderer.autoClear = true;
    // light
    //light.position.set(0, 20, 0);
    light.position.set(1, 1.5, 1);

    // boxhelper
    boxhelper.material.color= new THREE.Color(0x002266);

    // orbitcontrol define
    let orbitControl = new OrbitControls( builder.camera, renderer.domElement );
    orbitControl.update();
    orbitControl.addEventListener( 'change', render );

    new THREE.TextureLoader().load(
        // resource URL
        'static/asset/icons/check_icon.png',
        function ( texture ) {
            // in this example we create the material when the texture is loaded
            let material = new THREE.MeshBasicMaterial({map: texture, side:THREE.DoubleSide, color: 0x66ffff});
            let geometry = new THREE.PlaneGeometry(0.2, 0.2);
            material.polygonOffset = true;
            material.polygonOffsetFactor = -0.5;
            mouseHelper = new THREE.Mesh( geometry, material );
            mouseHelper.visible = false;
        }
    );

    // mouse helper Line
    const buffergeometry = new THREE.BufferGeometry();
    buffergeometry.setFromPoints( [ new THREE.Vector3(), new THREE.Vector3() ] );

    mouseHelperLine = new THREE.Line( buffergeometry, new THREE.LineBasicMaterial() );
    mouseHelperLine.visible = false;

    // grid helper
    let grid = new THREE.GridHelper( 30, 30, 0x444444, 0x888888 );

    // transform contorls define events
    let transformControls = new TransformControls( camera, renderer.domElement );
    // transformControls.setSize(0.1);

    transformControls.addEventListener( 'change', function () {
        let object = transformControls.object;

        if (object !== undefined) {

            boxhelper.setFromObject(object);

            builder.events.updateTransformInput.dispatch(object);

        }
        render();
    });


    transformControls.addEventListener('mouseDown', function () {
        orbitControl.enabled = false;
    });

    transformControls.addEventListener('mouseUp', function () {
        let object = transformControls.object;
        console.log('object Position ->', object.position);

        let camera = builder.camera;
        if (camera instanceof THREE.PerspectiveCamera){
            orbitControl.enabled = true;
        }
    });

    // events define callback
    events.renderStart.add ( function () {
        renderer.autoClear = false;
        renderer.autoUpdateScene = false;
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setClearColor( new THREE.Color(0x7EFFFF, 1.0) );
        builder.setSize( container.ele.offsetWidth, container.ele.offsetHeight );
        container.ele.appendChild(renderer.domElement);

        render();
    });

    events.windowResize.add ( function () {
        builder.setSize( container.ele.offsetWidth, container.ele.offsetHeight );

        render();
    });

    events.objectAdded.add( function (object) {
        console.log('objects', object);
        render();
    });

    events.objectRemoved.add( function (object) {
        transformControls.detach();
        console.log('objects', objects);
        render();
    });

    events.objectSelected.add ( function (object) {
        transformControls.detach();
        if (object !== null && object.name !== 'Target') {

            boxhelper.material.depthTest = false;
            boxhelper.material.transparent = true;

            boxhelper.setFromObject(object);
            boxhelper.visible = true;
            transformControls.attach(object);

            if(object.name === 'Video') {
                animation();
            } else {
                if (animationId !== null) {
                    //cancelAnimationFrame( animationId );
                    animationId = null;
                }
                render();
            }
        } else {
            if (animationId !== null) {
                //cancelAnimationFrame( animationId );
                animationId = null;
            }
            boxhelper.visible = false;
            render();
        }

    });

    events.objectChanged.add( function (object) {
        if (builder.selectedObject === object) {
            boxhelper.setFromObject(object);
        }
        render();
    });

    events.sceneChanged.add( function () {
        let scene = builder.scene;
        builder.select(null);
        // scene add
        scene.add(light);
        scene.add(grid);
        scene.add(boxhelper);
        scene.add(transformControls);

        render();
    } );
    events.sceneAdded.add( function (scene) {
        render();
    } );

    events.transformModeChanged.add( function ( mode ) {
        transformControls.setMode( mode );
    } );

    events.animationStarted.add ( function () {
        animation();
    });

    events.animationStopped.add ( function () {
        render();
    });

    events.cameraChanged.add ( function (camera) {
        builder.setCamara(camera);

        builder.select(null);
        transformControls.camera = camera;
        orbitControl.object = camera;

        transformControls.setMode('translate');

        if (camera instanceof THREE.OrthographicCamera) {
            orbitControl.enabled = false;
            events.transformModeChanged.dispatch('translate');
        } else if (camera instanceof THREE.PerspectiveCamera){
            orbitControl.enabled = true;
            events.transformModeChanged.dispatch('translate');
        }
        transformControls.updateMatrixWorld();
        orbitControl.update();
        render();
    });

    events.pointerModeStarted.add( function () {
        builder.rayMode = 'pointer';
        builder.select(null);
        boxhelper.visible = false;
        transformControls.detach();

        scene.add( mouseHelper );
        scene.add( mouseHelperLine );

        container.onMouseMove(onMouseMove);
        container.setStyle({cursor: 'alias'});
        animation();
    });

    events.pointerModeEnded.add( function () {
        builder.rayMode = 'init';
        scene.remove( mouseHelper );
        scene.remove( mouseHelperLine );

        container.removeEventListener('pointermove', onMouseMove);
        container.setStyle({cursor: 'default'});
        cancelAnimationFrame( animationId );
        render();
    });

    // scene add
    scene.add(light);
    scene.add(grid);
    scene.add(boxhelper);
    scene.add(transformControls);

    // raycaster set
    let raycaster = new THREE.Raycaster();
    let mouse = new THREE.Vector2();
    let onMouseDownPosition = new THREE.Vector2();
    let onMouseUpPosition = new THREE.Vector2();

    // mouse Events and Picking
    function checkIntersectionPointer (mouse) {
        let intersectObjects = getIntersectObjects(mouse);
        let intersectObject = ( intersectObjects.length ) > 0 ? intersectObjects[ 0 ] : null;

        if ( intersectObject ) {
            mouseHelper.visible = true;
            mouseHelperLine.visible = true;

            const p = intersectObject.point;
            const n = intersectObject.face.normal.clone();
            n.transformDirection( intersectObject.object.matrixWorld );
            // n.multiplyScalar( 10 );
            n.add( p );

	        console.log(mouseHelper.position);
            mouseHelper.position.copy( p );
            mouseHelper.lookAt(n);
            const positions = mouseHelperLine.geometry.attributes.position;
            positions.setXYZ( 0, p.x, p.y, p.z );
            positions.setXYZ( 1, n.x, n.y, n.z );
            positions.needsUpdate = true;
        } else {
        }
    }
    function getMousePosition (container, event) {
        let rect = container.ele.getBoundingClientRect(); // 전체 요소를 포함 하는 경계 박스
        let x = ( event.offsetX / rect.width ) * 2 - 1;
        let y = -( event.offsetY / rect.height ) * 2 + 1;
        return {x , y};
    }
    function getIntersectObjects (position) {
        let camera = builder.camera;

        let raytargets = objects.map((object) => {
            // return object.children[0];
            return object;
        });
        raycaster.setFromCamera(position, camera);
        return raycaster.intersectObjects( raytargets, true );
    }
    function getIntersectPosition (position) {
        let camera = builder.camera;
        raycaster.setFromCamera(position, camera);

        // ObitControl로 카메라 시선을 바꿔도 Plan이 항상 카메라에서 수직으로 존재 해야함
        let cameraPosition = camera.position.clone();
        let hPlane = new THREE.Plane(cameraPosition.normalize(), 0);
        let vPlane = new THREE.Plane(new THREE.Vector3(0, 1, 0), 0);

        let intersectPositionXY = raycaster.ray.intersectPlane(hPlane, new THREE.Vector3());
        let intersectPositionXZ = raycaster.ray.intersectPlane(vPlane, new THREE.Vector3());

        if(intersectPositionXY.y < 0 || camera instanceof THREE.OrthographicCamera){
            return intersectPositionXZ
        }
        return intersectPositionXY;
    }
    function onMouseDown (event) {
        container.focus();
        let position = getMousePosition(container, event);
        onMouseDownPosition.set(position.x, position.y);

        console.log('onMouseDownPosition', onMouseDownPosition);
        container.onMouseUp(onMouseUp);
    }

    function onMouseUp (event) {
        let position = getMousePosition(container, event);
        onMouseUpPosition.set(position.x, position.y);

        console.log('onMouseUpPosition', onMouseUpPosition);
        let intersectObjects = getIntersectObjects(onMouseUpPosition);

        console.log('mouse distance', onMouseDownPosition.distanceTo(onMouseUpPosition));

        if (onMouseDownPosition.distanceTo(onMouseUpPosition) <= 0.02) {
            if (intersectObjects.length > 0) {
                let object = intersectObjects[0].object;

                if (builder.rayMode === 'pointer') {
                    add3DPointer (object);
                } else {
                    if (builder.selectedObject !== object) {
                        if (object.parent !== null && !(object.parent instanceof THREE.Scene)) {
                            builder.select(object.parent);
                            // builder.select(object.parent);
                        }
                    }
                }
            } else {
                builder.select();
            }
        }

        container.removeEventListener('pointerup', onMouseUp);
    }

    function add3DPointer(object) {
    	console.log('add3DPointer', mouseHelper.position)
        let pointer = mouseHelper.clone();
        pointer.name = 'pointer';
        // object.parent.add(pointer);
        builder.execute(new Add3DPointerCommand(pointer, object.parent));
    }

    function onMouseMove (event) {
        console.log('onMouseMove')
        event.preventDefault();
        container.focus();
        let position = getMousePosition(container, event);
        checkIntersectionPointer(position);
    }

    container.onMouseDown(onMouseDown);

    // object add by draggig
    container.onDragEnter(function () {
        container.setStyle({'border': '2px dotted #00D8FF'})
    });
    container.onDragOver(function (event) {
        event.preventDefault();
        container.setStyle({'border': '2px dotted #DCEEF2'})
    });
    container.onDragLeave(function () {
        container.setStyle({'border': '2px solid transparent'});
    });
    container.onDrop(function () {

        container.setStyle({'border': '2px solid transparent'});

        if (builder.draggingObject !== null) {
            let mousePos = getMousePosition(container, event);
            let pos = getIntersectPosition(mousePos);
            //builder.draggingObject.position.copy(pos);
            let command = new AddObjectCommand(builder.draggingObject, pos);
            console.log('dragging object pos', builder.draggingObject.position);
            builder.execute(command);
        }
    });

    // keyboard keydown events

    document.addEventListener('keydown', onKeyDown);

    function onKeyDown(event){
        let key = event.key.toLowerCase();
        let object = builder.selectedObject;
        if(object === null) return;

        switch (key) {
            case 'delete':
                boxhelper.visible = false;
                if(object.name === 'pointer') {
                    builder.execute(new Remove3DPointerCommand(object));
                } else {
                    builder.execute(new RemoveObjectCommand(object));
                }
                break;
        }
    }

    function render() {
        let camera = builder.camera;
        let scene = builder.scene;
        renderer.render(scenes[scene.uuid], camera);
    }

    function animation() {
        // render using requestAnimationFrame
        let mixer = builder.mixer;
        let delta = clock.getDelta();

        let needsUpdate = false;
        animationId = requestAnimationFrame(animation);

        if ( mixer.stats.actions.inUse > 0 ) {
            mixer.update( delta );
            needsUpdate = true;
        }

        render()
    }

    return container;
};

export default WebGLView;
