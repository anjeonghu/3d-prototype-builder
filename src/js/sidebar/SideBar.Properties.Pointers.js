import SideBar from './SideBar';
import Container from '../Container';

SideBar.Properties.Pointers = function (builder) {
    let events = builder.events;
    let pointers = builder.pointers;
    let selectedObject;
    let pointersContainer = new Container.Section('3D Pointers');
    let pointersBody = new Container.DIV().setClass('section-body');

    pointersContainer.add(pointersBody);

    events.objectSelected.add(function () {
        selectedObject = builder.selectedObject;
        if (selectedObject) {
            if (selectedObject.name === '3d') {
                pointersBody.add(make3DPointerList(selectedObject.uuid));
            }
        }
    });
    events.objectRemoved.add(function (object) {
    });

    function make3DPointerList (uuid) {
        // pointersBody clear
        pointersBody.clear();
        // pointers list
        let pointerList = pointers[uuid] || [];
        let container = new Container.DIV().setClass('pointers-list');
        pointerList.forEach(function (pointer, index) {
            let pointersDiv = new Container.DIV().setTextContent(`# 점검부위 - ${index + 1}`);
            pointersDiv.setAttribute('data', pointer.uuid).setStyle({cursor: 'pointer'});
            pointersDiv.onClick(onPointerSelected);
            container.add(pointersDiv);
        });

        return container;
    }

    function onPointerSelected () {
        let uuid = this.getAttribute('data');
        let pointer = builder.getObject(uuid);

        builder.select(pointer);
    }

    return pointersContainer;
};

export default SideBar.Properties.Pointers