import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Geometries = function (builder) {
    let container = new Container.DIV();
    let boxGeometryDraggable = new Container.DraggableContent('Cube');
    let cylinderDraggable = new Container.DraggableContent('Cylinder');
    let coneDraggable = new Container.DraggableContent('Cone');
    let sphereDraggable = new Container.DraggableContent('Sphere');

    container.setClass('geometries');

    boxGeometryDraggable.onDragStart(function () {
        let geometry = new THREE.BoxBufferGeometry( 1, 1, 1, 1, 1, 1 ); //  width, height, depth, widthSegments, heightSegments, depthSegments
        let mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial({ color: 0x00D8FF, side: THREE.DoubleSide }) );
        mesh.name = 'Cube';
        builder.draggingObject = mesh;
    });
    cylinderDraggable.onDragStart(function () {
        let geometry =  new THREE.CylinderBufferGeometry( 0.5, 0.5, 0.1, 32 ); //  radiusTop, radiusBottom, height, radialSegments, heightSegments
        let mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial({ color: 0x00D8FF, side: THREE.DoubleSide }) );
        mesh.name = 'Cylinder';
        builder.draggingObject = mesh;
    });
    coneDraggable.onDragStart(function () {
        let geometry = new THREE.ConeBufferGeometry( 1, 2, 32 ); // radius, height, radialSegments, heightSegments
        let mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial({ color: 0x00D8FF, side: THREE.DoubleSide }) );
        mesh.name = 'Cone';
        builder.draggingObject = mesh;
    });
    sphereDraggable.onDragStart(function () {
        let geometry = new THREE.SphereBufferGeometry( 1, 32, 32 );  //  radius, widthSegments, heightSegments
        let mesh = new THREE.Mesh( geometry, new THREE.MeshLambertMaterial({ color: 0x00D8FF, side: THREE.DoubleSide }) );
        mesh.name = 'Sphere';
        builder.draggingObject = mesh;
    });


    container.add(boxGeometryDraggable);
    container.add(cylinderDraggable);
    container.add(coneDraggable);
    container.add(sphereDraggable);

    return container;
};

export default SideBar.Geometries;