import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Widgets = function (builder) {
    let container = new Container.DIV();
    let textDraggable = new Container.DraggableContent('Text');
    let videoDraggable = new Container.DraggableContent('Video');
    let ImageDraggable = new Container.DraggableContent('Image');

    // texture
     let imageIconTexture = new THREE.TextureLoader().load('https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D_Specs.png');
     //let imageIconTexture = new THREE.TextureLoader().load('static/asset/texture/image-icon.png');

    container.setClass('widgets');


    // draggable event
    textDraggable.onDragStart(function () {
        let mesh = createLabel('sample text');
        mesh.name = 'Text';
        builder.draggingObject = mesh;
    });

    videoDraggable.onDragStart(function () {
        let mesh = createVideo();
        mesh.name = 'Video';
        // mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        builder.draggingObject = mesh;
    });

    ImageDraggable.onDragStart(function () {
        let mesh = createImage();
        mesh.name = 'Image';
        // mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        builder.draggingObject = mesh;
    });


    // canvas text texture
    function createLabel(text) {
        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        context.font = "Bold 45px Arial";
        context.fillStyle = "black";
        context.strokeStyle  = "black";

        let textWidth = context.measureText(text).width;
        context.fillText(text, (canvas.width / 2) - (textWidth / 2), canvas.height / 2);

        // canvas contents will be used for a texture
        let texture= new THREE.Texture(canvas);
        texture.needsUpdate = true;

        let material = new THREE.MeshBasicMaterial( {map: texture, side:THREE.DoubleSide } );
        material.transparent = true;

        let mesh = new THREE.Mesh(new THREE.PlaneGeometry(canvas.width / (canvas.height * 10), 0.1), material);
        mesh.overdraw = true;
        mesh.doubleSided = true;


        mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        mesh.name = 'Text';
        mesh.userData.value = text;
        mesh.userData.texture =  {
            canvasContext: context,
            font: {
                color: { r: 0, g: 0, b: 0}
            }
        };
        mesh.userData.origin =  {textWidth: textWidth, canvasWidth: canvas.width};

        return mesh;
    }

    function createVideo () {
        let video = document.createElement('video');
        let source = document.createElement('source');

        source.type = 'video/mp4';
        source.src = 'https://raw.githubusercontent.com/happenask/staticStorage/master/videos/Hyundai_robotics_Streaming.mp4.m4v';
        video.autoplay = true;
        video.loop = 'loop';
        video.crossOrigin = "Anonymous"; // cross orin data는 이 설정이 필요

        video.appendChild(source);

        video.load();

        let texture = new THREE.VideoTexture(video);
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.format = THREE.RGBFormat;
        texture.needsUpdate = true;

        let material = new THREE.MeshBasicMaterial({map: texture, side:THREE.DoubleSide });
        let geometry = new THREE.PlaneGeometry(0.16, 0.09);

        let mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        mesh.name = 'Video';
        mesh.userData.srcUrl = 'https://raw.githubusercontent.com/happenask/staticStorage/master/videos/Hyundai_robotics_Streaming.mp4.m4v';

        return mesh;
    }

    function createImage () {
        let material = new THREE.MeshBasicMaterial({map: imageIconTexture, side:THREE.DoubleSide });
        let geometry = new THREE.PlaneGeometry(0.2, 0.1);
        let mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        mesh.name = 'Image';
        mesh.userData.srcUrl = 'https://raw.githubusercontent.com/happenask/staticStorage/master/images/2D_Specs.png';

        return mesh;
    }

    container.add(textDraggable);
    container.add(videoDraggable);
    container.add(ImageDraggable);

    return container;
};

export default SideBar.Widgets