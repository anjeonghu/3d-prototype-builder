import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import AddObjectCommand from '../command/AddObjectCommand';

SideBar.Import = function (builder) {
    let container = new Container.DIV().setClass('import');
    let realWidth = 3; // m 단위
    let realHeight = 3; // m 단위
    // let realWidth = 0.3; // m 단위
    // let realHeight = 0.3; // m 단위
    let geometry = new THREE.PlaneGeometry(0.3, 0.3);
    let material = null;
    // upload


    //image
    let targetBtn = new Container.Button('TARGET').setClass('btn-neon');
    let Imageloader = new THREE.TextureLoader();
    Imageloader.load('https://raw.githubusercontent.com/happenask/staticStorage/master/images/Robot.jpg', function ( image ) {
        let w = image.image.width;
        let h = image.image.height;
        realHeight =  ((h / w) * realWidth).toFixed(2);

        geometry = new THREE.PlaneGeometry(realWidth, realHeight);
        material = new THREE.MeshBasicMaterial({
            map: image
        });
    });

    targetBtn.onClick(addImage);

    // obj file
    let fileBtn = new Container.Input('file', 'MODEL').setClass('btn-neon');
    fileBtn.setAttribute('multiple', false);

    fileBtn.onChange(add3DModel);

    function addImage () {
        let mesh = new THREE.Mesh(geometry, material);
        mesh.rotation.x = builder.helper.getDegreeToRadian(-90);
        mesh.name = 'Target';
        let command = new AddObjectCommand(mesh);
        builder.execute(command);
    }

    function add3DModel (event) {
        builder.loader.load(fileBtn.getValue());
        fileBtn.setValue('');
    }
    container.add(targetBtn, fileBtn);

    return container;
};

export default SideBar.Import