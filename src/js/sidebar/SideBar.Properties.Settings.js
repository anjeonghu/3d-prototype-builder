import * as THREE from 'three.module'
import {colors} from '../Const';
import SideBar from './SideBar';
import Container from '../Container';
import SetTextureCommand from '../command/SetTextureCommand';

SideBar.Properties.Settings = function (builder) {
    let events = builder.events;
    let object = builder.selectedObject;

    let settingContainer = new Container.Section('Setting');
    let settingBody = new Container.SectionBody();

    settingContainer.add(settingBody);

    // text Label
    let labelTextContainer = new Container.Property('Text Label');
    let labelTextBody = new Container.PropertyBody();
    let inputTextLabel = new Container.Input('text', 'Label').onChange(updateLabel);
    labelTextBody.add(inputTextLabel);
    labelTextContainer.add(labelTextBody);
    // //text color
    let labelRGBContainer = new Container.Property('Text Color');
    let labelRGBBody = new Container.PropertyBody();

    Object.keys(colors).forEach(function (key, index) {
        let colorspan = new Container.SPAN().setStyle({'background-color': colors[key].hex}).setAttribute('data', key);
        colorspan.onClick(updateLabelColor);

        labelRGBBody.add(colorspan);
    });

    labelRGBContainer.add(labelRGBBody);

    // image url input
    let imageContainer = new Container.Property('Image Url');
    let imageBody = new Container.PropertyBody();
    let inputImageUrl = new Container.Input('text', '').onChange(updateImageTexture);

    imageBody.add(inputImageUrl);
    imageContainer.add(imageBody);


    // video url input
    let videoContainer = new Container.Property('Video Url');
    let videoBody = new Container.PropertyBody();
    let inputVideoUrl = new Container.Input('text', '').onChange(updateVideoTexture);

    videoBody.add(inputVideoUrl);
    videoContainer.add(videoBody);

    function updateLabel () {
        object = builder.selectedObject;
        let originTextWidth = object.userData.origin.textWidth;
        let originCanvasWidth = object.userData.origin.canvasWidth;
        let newText = this.value;
        let canvas = object.userData.texture.canvasContext.canvas;
        let canvasContext = object.userData.texture.canvasContext;
        let font = canvasContext.font;
        let fillStyle = canvasContext.fillStyle;
        let newTextWidth = canvasContext.measureText(newText).width;
        let scaleRatio = (newTextWidth / originTextWidth);
        canvas.width = originCanvasWidth * scaleRatio;
        canvasContext.font = font;
        canvasContext.fillStyle = fillStyle;

        canvasContext.fillText(newText, (canvas.width / 2) - (newTextWidth / 2), canvas.height / 2);

        let texture= new THREE.Texture(canvas);
        texture.needsUpdate = true;

        object.userData.value = newText;
        object.userData.texture.canvas = canvas;
        object.userData.texture.canvasContext = canvasContext;

        object.scale.x = scaleRatio;
        builder.execute(new SetTextureCommand(object, texture));
    }

    function updateLabelColor (){
        object = builder.selectedObject;
        let colorKey = this.getAttribute('data');
        let textValue = object.userData.value;
        let canvas = object.userData.texture.canvasContext.canvas;
        let canvasContext = object.userData.texture.canvasContext;
        let textWidth = canvasContext.measureText(textValue).width;

        canvasContext.save();
        canvasContext.clearRect(0, 0, canvas.width, canvas.height);

        canvasContext.fillStyle = colors[colorKey].hex;

        canvasContext.fillText(textValue, (canvas.width / 2) - (textWidth / 2), canvas.height / 2);

        canvasContext.restore();
        let texture= new THREE.Texture(canvas);
        texture.needsUpdate = true;

        object.userData.texture.font.color = colors[colorKey].rgb;
        builder.execute(new SetTextureCommand(object, texture));
    }

    function updateImageTexture () {
        object = builder.selectedObject;
        let newUrl = this.value;
        let loader = new THREE.TextureLoader();
        loader.load(newUrl, function (texture ) {
                let w = texture.image.width;
                let h = texture.image.height;

                let { x, y} = (w > h) ? {x: w / (h * 10), y: 0.1} : {x: 0.1, y: h / (w * 10)};

                object.children[0].geometry = new THREE.PlaneGeometry(x.toFixed(2), y.toFixed(2));

                object.userData.srcUrl = newUrl;

                builder.execute(new SetTextureCommand(object, texture ));
            },
            undefined,
            // onError callback
            function ( err ) {
                console.error( "image doesn't exist!!!");
            }
        );
    }


    function updateVideoTexture () {
        object = builder.selectedObject;
        let newUrl = this.value;
        let video = document.createElement('video');
        let source = document.createElement('source');

        source.type = 'video/mp4';
        source.src = newUrl;
        video.autoplay = true;
        video.loop = 'loop';
        video.crossOrigin = "Anonymous";

        video.appendChild(source);

        video.load();

        let texture = new THREE.VideoTexture(video);
        texture.minFilter = THREE.LinearFilter;
        texture.magFilter = THREE.LinearFilter;
        texture.format = THREE.RGBFormat;
        texture.needsUpdate = true;


        video.addEventListener( "loadedmetadata", function (e) {
            let w = texture.image.videoWidth;
            let h = texture.image.videoHeight;

            let { x, y} = (w > h) ? {x: w / (h * 10), y: 0.1} : {x: 0.1, y: h / (w * 10)};

            object.children[0].geometry = new THREE.PlaneGeometry(x.toFixed(2), y.toFixed(2));
            object.userData.srcUrl = newUrl;
            builder.execute(new SetTextureCommand(object, texture ));

        }, false );
    }

    events.objectSelected.add(function () {
        object = builder.selectedObject;
        if (object !== null ) {
            switch (object.name) {
                case 'Text':
                    settingContainer.setStyle({display: 'block'});
                    settingBody.innerElement(labelTextContainer, labelRGBContainer);
                    inputTextLabel.setValue(object.userData.value);
                    break;
                case 'Image':
                    settingContainer.setStyle({display: 'block'});
                    settingBody.innerElement(imageContainer);
                    inputImageUrl.setValue(object.userData.srcUrl || '');
                    break;
                case 'Video':
                    settingContainer.setStyle({display: 'block'});
                    settingBody.innerElement(videoContainer);
                    inputVideoUrl.setValue(object.userData.srcUrl || '');
                    break;
                default:
                    settingBody.innerHTML('No Action');
                    break;
            }

        }
    });


    return settingContainer;
};

export default SideBar.Properties.Settings