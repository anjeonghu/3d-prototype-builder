import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import SetScaleCommand from "../command/SetScaleCommand";

SideBar.Animation.Translate = function (builder) {
    let events = builder.events;
    let sessionContainer = new Container.Section('Translate');
    let sessionBody = new Container.DIV().setClass('section-body');

    sessionContainer.add(sessionBody);

    // position
    let positionBody = new Container.PropertyBody().setClass('grid-column-3');

    let inputXt = new Container.Input('number', 'X')
        .setValue(0)
        .onChange(updateTransForm);
    let inputYt = new Container.Input('number', 'Y')
        .setValue(0)
        .onChange(updateTransForm);
    let inputZt = new Container.Input('number', 'Z')
        .setValue(0)
        .onChange(updateTransForm);

    positionBody.add(inputXt);
    positionBody.add(inputYt);
    positionBody.add(inputZt);

    // play time
    let playTimeBody = new Container.PropertyBody();
    let inputPlayTime = new Container.Input('number', 'Play Time')
        .setValue(1)
        .setAttribute('step', 1)
        .onChange();

    playTimeBody.add(inputPlayTime);

    sessionBody.add(positionBody);
    sessionBody.add(playTimeBody);

    function updateTransForm () {
        let object = builder.selectedObject;
        if (object === null) return;

        let xt = Number(inputXt.getValue());
        let yt = Number(inputYt.getValue());
        let zt = Number(inputZt.getValue());
        let translate = new THREE.Vector3(xt, yt, zt);
     //   builder.execute(new SetScaleCommand(object, scale));
    }

    return sessionContainer;
};

export default SideBar.Animation.Translate