
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Animation = function (builder) {
    let events = builder.events;
    let container = new Container.DIV();
	let sequenceProperties = new SideBar.Animation.Sequence(builder);
	let translateProperties = new SideBar.Animation.Translate(builder);
	let scaleProperties = new SideBar.Animation.Scale(builder);
	let rotationProperties = new SideBar.Animation.Rotation(builder);


    events.objectSelected.add(function () {
        let object = builder.selectedObject;
    });

    events.addAnimationTranslate.add(function () {
        let object = builder.selectedObject;
        container.add(translateProperties)
    });

    events.addAnimationScale.add(function () {
        let object = builder.selectedObject;
        container.add(scaleProperties)
    });

    events.addAnimationRotation.add(function () {
        let object = builder.selectedObject;
        container.add(rotationProperties)
    });

    events.removeAnimationTranslate.add(function () {
        let object = builder.selectedObject;
    });

    events.removeAnimationScale.add(function () {
        let object = builder.selectedObject;
    });

    events.removeAnimationRotation.add(function () {
        let object = builder.selectedObject;
    });

    container.add(sequenceProperties);

    return container;
};

export default SideBar.Animation;