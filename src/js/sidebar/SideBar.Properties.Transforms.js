import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import SetPositionCommand from '../command/SetPositionCommand';
import SetRotationCommand from '../command/SetRotationCommand';
import SetScaleCommand from '../command/SetScaleCommand';

SideBar.Properties.Transforms = function (builder) {
    let events = builder.events;
    let transformContainer = new Container.Section('Transform');
    let transformBody = new Container.DIV().setClass('transform-body');

    transformContainer.add(transformBody);

    // position
    let positionContainer = new Container.Property('Position');
    let positionBody = new Container.PropertyBody();
    let inputXt = new Container.Input('number', 'X').onChange(updateTransForm);
    let inputYt = new Container.Input('number', 'Y').onChange(updateTransForm);
    let inputZt = new Container.Input('number', 'Z').onChange(updateTransForm);

    positionBody.add(inputXt);
    positionBody.add(inputYt);
    positionBody.add(inputZt);
    positionContainer.add(positionBody );

    // rotation
    let rotationContainer = new Container.Property('Rotation');
    let rotationBody = new Container.PropertyBody();
    let inputXr = new Container.Input('number', 'X').onChange(updateTransForm);
    let inputYr = new Container.Input('number', 'Y').onChange(updateTransForm);
    let inputZr = new Container.Input('number', 'Z').onChange(updateTransForm);

    rotationBody.add(inputXr);
    rotationBody.add(inputYr);
    rotationBody.add(inputZr);
    rotationContainer.add(rotationBody );

    // scale
    let scaleContainer = new Container.Property('Scale');
    let scaleBody = new Container.PropertyBody();
    let inputXs = new Container.Input('number', 'X').onChange(updateTransForm);
    let inputYs = new Container.Input('number', 'Y').onChange(updateTransForm);
    let inputZs = new Container.Input('number', 'Z').onChange(updateTransForm);

    scaleBody.setClass('property-body');
    scaleBody.add(inputXs);
    scaleBody.add(inputYs);
    scaleBody.add(inputZs);
    scaleContainer.add(scaleBody );

    
    function updateTransForm () {
        let object = builder.selectedObject;
        if (object === null) return;


        let xt = Number(inputXt.getValue());
        let yt = Number(inputYt.getValue());
        let zt = Number(inputZt.getValue());

        let position = new THREE.Vector3(xt, yt, zt);
        builder.execute(new SetPositionCommand(object, position));

        let xr = Number(inputXr.getValue());
        let yr = Number(inputYr.getValue());
        let zr = Number(inputZr.getValue());

        let rotation = new THREE.Euler(builder.helper.getDegreeToRadian(xr), builder.helper.getDegreeToRadian(yr), builder.helper.getDegreeToRadian(zr));
        builder.execute(new SetRotationCommand(object, rotation));
        //

        let xs = Number(inputXs.getValue());
        let ys = Number(inputYs.getValue());
        let zs = Number(inputZs.getValue());
        let scale = new THREE.Vector3(xs, ys, zs);
        builder.execute(new SetScaleCommand(object, scale));
    }

    function updateInputField (object) {
        inputXt.setValue(object.position.x);
        inputYt.setValue(object.position.y);
        inputZt.setValue(object.position.z);

        inputXr.setValue(builder.helper.getRadianToDegree(object.rotation.x));
        inputYr.setValue(builder.helper.getRadianToDegree(object.rotation.y));
        inputZr.setValue(builder.helper.getRadianToDegree(object.rotation.z));

        inputXs.setValue(object.scale.x);
        inputYs.setValue(object.scale.y);
        inputZs.setValue(object.scale.z);
    }

    events.objectSelected.add(function () {
        let object = builder.selectedObject;

        if (object !== null ) {
            updateInputField(object);

            let camera = builder.camera;
            if (camera instanceof THREE.OrthographicCamera) {
                inputYt.setAttribute('disabled', 'disabled');
                inputYs.setAttribute('disabled', 'disabled');
                inputXr.setAttribute('disabled', 'disabled');
                inputYr.setAttribute('disabled', 'disabled');
                inputZr.setAttribute('disabled', 'disabled');
            } else {
                inputYt.removeAttribute('disabled');
                inputYs.removeAttribute('disabled');
                inputXr.removeAttribute('disabled');
                inputYr.removeAttribute('disabled');
                inputZr.removeAttribute('disabled');
            }
        }
    });

    events.updateTransformInput.add(function (object) {
        if (builder.selectedObject !== object) return;
        updateInputField(object);
    });

    transformBody.add(positionContainer);
    transformBody.add(rotationContainer);
    transformBody.add(scaleContainer);

    return transformContainer;
};

export default SideBar.Properties.Transforms