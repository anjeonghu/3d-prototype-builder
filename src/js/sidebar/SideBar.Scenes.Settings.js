import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import AddSceneCommand from '../command/AddSceneCommand';

SideBar.Scenes.Settings = function (builder) {
    let events = builder.events;
    let settingContainer = new Container.Section('Setting');
    let settingBody = new Container.DIV().setClass('section-body');

    settingContainer.add(settingBody);


    // add Scene
    let addSceneContainer = new Container.Property('Add Scene');
    let addSceneBody = new Container.PropertyBody();
    let inputScene = new Container.Input('text', '').setPlaceHolder('type scene name');
    let addSceneBtn = new Container.Button('ADD').setClass('btn-neon').onClick(onAddScene);

    addSceneBody.add(inputScene);
    addSceneBody.add(addSceneBtn);
    addSceneContainer.add(addSceneBody);

    events.sceneChanged.add(function () {

    });
    let scenesContainer = new Container.Property('Scenes');
    let scenesBody = new Container.PropertyBody();

    function makeSceneList () {
        let scenes = builder.scenes;
        // scene list

        let container = new Container.DIV().setClass('scenes-list');
        Object.keys(scenes).forEach(function (key, index) {
            let sceneDiv = new Container.DIV().setTextContent(scenes[key].name);
            sceneDiv.setAttribute('data', key);
            sceneDiv.onClick(onSceneSelected);
            container.add(sceneDiv);
        });

        return container;
    }

    scenesBody.add(makeSceneList());
    scenesContainer.add(scenesBody);

    function onSceneSelected () {
        let suuid = this.getAttribute('data');
        let scene = builder.scenes[suuid];

        if (scene !== builder.scene) {
            builder.setScene(scene);
            events.sceneChanged.dispatch();
        }
    }

    function onAddScene () {
        let scene = new THREE.Scene();
        scene.name = inputScene.getValue();
        builder.execute(new AddSceneCommand(scene));
        scenesContainer.innerElement(makeSceneList());
    }

    settingBody.add(scenesContainer);
    settingBody.add(addSceneContainer);
    return settingContainer;
};

export default SideBar.Scenes.Settings