import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import SetScaleCommand from "../command/SetScaleCommand";
import SetRotationCommand from "../command/SetRotationCommand";

SideBar.Animation.Rotation = function (builder) {
    let events = builder.events;
    let sectionContainer = new Container.Section('Rotation');
    let sectionBody = new Container.DIV().setClass('section-body');

    sectionContainer.add(sectionBody);

    // rotation
    let rotationBody = new Container.PropertyBody().setClass('grid-column-3');

    let inputXr = new Container.Input('number', 'X')
        .setValue(0)
        .setAttribute('step', 1)
        .onChange(updateTransForm);
    let inputYr = new Container.Input('number', 'Y')
        .setValue(0)
        .setAttribute('step', 1)
        .onChange(updateTransForm);
    let inputZr = new Container.Input('number', 'Z')
        .setValue(0)
        .setAttribute('step', 1)
        .onChange(updateTransForm);

    rotationBody.add(inputXr);
    rotationBody.add(inputYr);
    rotationBody.add(inputZr);

    // play time
    let playTimeBody = new Container.PropertyBody();
    let inputPlayTime = new Container.Input('number', 'Play Time')
        .setValue(1)
        .setAttribute('step', 1)
        .onChange();

    playTimeBody.add(inputPlayTime);

    sectionBody.add(rotationBody);
    sectionBody.add(playTimeBody);

    function updateTransForm () {
        let object = builder.selectedObject;
        if (object === null) return;

        let xr = Number(inputXr.getValue());
        let yr = Number(inputYr.getValue());
        let zr = Number(inputZr.getValue());

        let rotation = new THREE.Euler(builder.helper.getDegreeToRadian(xr), builder.helper.getDegreeToRadian(yr), builder.helper.getDegreeToRadian(zr));
        builder.execute(new SetRotationCommand(object, rotation));
    }

    return sectionContainer;
};

export default SideBar.Animation.Rotation