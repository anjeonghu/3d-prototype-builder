
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Properties = function (builder) {
    let events = builder.events;
    let container = new Container.DIV();
	let settingsProperties = new SideBar.Properties.Settings(builder).setStyle({display: 'none'});
	let transformProperties = new SideBar.Properties.Transforms(builder).setStyle({display: 'none'});
    let animationsProperties = new SideBar.Properties.Animations(builder).setStyle({display: 'none'});
    let pointersProperties = new SideBar.Properties.Pointers(builder).setStyle({display: 'none'});

    events.objectSelected.add(function () {
        let object = builder.selectedObject;
        if (object !== null && object.name !== 'Target') {
            transformProperties.setStyle({display: 'block'});
            settingsProperties.setStyle({display: 'block'});

            if (object.animations.length > 0) {
	            animationsProperties.setStyle({display: 'block'});
            } else {
	            animationsProperties.setStyle({display: 'none'});
            }
            if (builder.pointers[object.uuid] && builder.pointers[object.uuid].length > 0) {
	            pointersProperties.setStyle({display: 'block'})
            } else {
	            pointersProperties.setStyle({display: 'none'})
            }
        } else {
	        transformProperties.setStyle({display: 'none'});
	        settingsProperties.setStyle({display: 'none'});
	        animationsProperties.setStyle({display: 'none'});
	        pointersProperties.setStyle({display: 'none'})
        }
    });

    container.add(settingsProperties);
    container.add(transformProperties);
    container.add(animationsProperties);
    container.add(pointersProperties);

    return container;
};

export default SideBar.Properties;