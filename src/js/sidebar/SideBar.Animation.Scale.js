import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import SetPositionCommand from "../command/SetPositionCommand";
import SetRotationCommand from "../command/SetRotationCommand";
import SetScaleCommand from "../command/SetScaleCommand";

SideBar.Animation.Scale = function (builder) {
    let events = builder.events;
    let sectionContainer = new Container.Section('Scale');
    let sectionBody = new Container.DIV().setClass('section-body');

    sectionContainer.add(sectionBody);
    // scale
    let scaleBody = new Container.PropertyBody().setClass('grid-column-3');

    let inputXs = new Container.Input('number', 'X')
        .setValue(0)
        .onChange(updateTransForm);
    let inputYs = new Container.Input('number', 'Y')
        .setValue(0)
        .onChange(updateTransForm);
    let inputZs = new Container.Input('number', 'Z')
        .setValue(0)
        .onChange(updateTransForm);

    scaleBody.add(inputXs);
    scaleBody.add(inputYs);
    scaleBody.add(inputZs);

    // play time
    let playTimeBody = new Container.PropertyBody();
    let inputPlayTime = new Container.Input('number', 'Play Time')
        .setValue(1)
        .setAttribute('step', 1)
        .onChange();

    playTimeBody.add(inputPlayTime);

    sectionBody.add(scaleBody);
    sectionBody.add(playTimeBody);

    function updateTransForm () {
        let object = builder.selectedObject;
        if (object === null) return;

        let xs = Number(inputXs.getValue());
        let ys = Number(inputYs.getValue());
        let zs = Number(inputZs.getValue());
        let scale = new THREE.Vector3(xs, ys, zs);
        builder.execute(new SetScaleCommand(object, scale));
    }

    return sectionContainer;
};

export default SideBar.Animation.Scale