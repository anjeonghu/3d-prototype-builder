/**
 * Created by front on 2019-02-01.
 */
import Container from '../Container';

const SideBar = function (builder, components) {
    let container = new Container.SideBar();
    let tabPanel = new Container.TabPanel();
    let tabs = {};
    let objects = {};

    if (components === undefined) {
        return container;
    }

    container.add(tabPanel);

    for (let i = 0; i < components.length; i++) {
        let component = components[i];
        let tab = new Container.Tab(component);
        tabPanel.add(tab);
        tab.onClick(onClick);
        if (i === 0 ) {
            tab.setClass('selected')
        }
        tabs[component] = tab;

        if(typeof SideBar[component] !== 'function') {
            console.error(component + " doesn't exist");;
        } else {
            let obj = new SideBar[component](builder);
            if (i > 0) {
                obj.setStyle({'display': 'none'})
            }
            objects[component] =obj;
            container.add(obj);
        }
    }

    function onClick (event) {
        select (event.target.textContent);
    }

    function select (selection) {
        Object.keys(tabs).forEach((key) => {
            if (key === selection) {
                tabs[key].setClass('selected');
            } else {
                tabs[key].removeClass('selected');
            }
        });

        Object.keys(objects).forEach((key)=>{
            if (key === selection) {
                objects[key].setStyle({'display': ''})
            } else {
                objects[key].setStyle({'display': 'none'})
            }
        })
    }
    return container;
};

export default SideBar;