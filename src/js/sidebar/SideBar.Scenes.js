
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Scenes = function (builder) {
    let events = builder.events;
    let container = new Container.DIV();
    let settingsProperties = new SideBar.Scenes.Settings(builder);


    events.sceneChanged.add(function () {

    });

    container.add(settingsProperties);

    return container;
};

export default SideBar.Scenes;