import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';

SideBar.Animation.Sequence = function (builder) {
    let events = builder.events;
    let sequenceContainer = new Container.Section('Sequence');
    let sequenceBody = new Container.DIV().setClass('section-body');

    sequenceContainer.add(sequenceBody);

    // add Scene Name
    let addNameContainer = new Container.Property('Name');
    let addNameBody = new Container.PropertyBody();
    let inputName = new Container.Input('text', '').setPlaceHolder('type name');

    addNameBody.add(inputName);
    addNameContainer.add(addNameBody);

    // sequence components buttons
    let componentsContainer = new Container.Property('Components');
    let componentsBody = new Container.PropertyBody();
    let translateBtn = new Container.Button('TRANSLATE').setClass('btn-neon').onClick(onAddTranslate);
    let scaleBtn = new Container.Button('SCALE').setClass('btn-neon').onClick(onAddScale);
    let rotationBtn = new Container.Button('ROTATION').setClass('btn-neon').onClick(onAddRotation);

    componentsBody.add(translateBtn);
    componentsBody.add(scaleBtn);
    componentsBody.add(rotationBtn);
    componentsContainer.add(componentsBody);

    // sequence play time
    let playTimeContainer = new Container.Property('Total Play Time');
    let playTimeBody = new Container.PropertyBody();
    let playTimeDisplay = new Container.DIV().setTextContent('0 s');

    playTimeBody.add(playTimeDisplay);
    playTimeContainer.add(playTimeBody);

    // sequence preview
    let playBtn = new Container.ImageBtn('static/asset/icons/play-icon.svg')
        .setClass('play-btn')
        .onClick(play);

    let previewContainer = new Container.Property('Preview');
    let previewBody = new Container.PropertyBody();

    previewBody.add(playBtn);
    previewContainer.add(previewBody);

    function onAddTranslate () {
        // let object = builder.selectedObject;
        // if (object === null) return;

	    events.addAnimationTranslate.dispatch();
    }

    function onAddScale () {
        events.addAnimationScale.dispatch();
    }

    function onAddRotation () {
        events.addAnimationRotation.dispatch();
    }

    function play () {

    }


    sequenceContainer.add(addNameContainer);
    sequenceContainer.add(componentsContainer);
    sequenceContainer.add(playTimeContainer);
    sequenceContainer.add(previewContainer);

    return sequenceContainer;
};

export default SideBar.Animation.Sequence