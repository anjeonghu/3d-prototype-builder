import * as THREE from 'three.module'
import SideBar from './SideBar';
import Container from '../Container';
import SetScaleCommand from '../command/SetScaleCommand';

SideBar.Properties.Animations = function (builder) {
    let events = builder.events;
    let mixer = builder.mixer;
    let animations ={};
    let animationsContainer = new Container.Section('Animations');
    let animationsBody = new Container.DIV().setClass('section-body');
    // animation button
    let animationSelectBox;
    let startBtn = new Container.Button('PLAY').setClass('btn-neon').onClick(playAnimation);
    let stopBtn = new Container.Button('STOP').setClass('btn-neon').onClick(stopAnimation);

    animationsContainer.add(animationsBody);

    function playAnimation () {
        events.animationStarted.dispatch();
        animations[animationSelectBox.getValue()].play();
    }

    function stopAnimation () {
        events.animationStopped.dispatch();
        animations[animationSelectBox.getValue()].stop();
    }

    events.objectSelected.add( function ( object ) {

        if ( object !== null && object.animations.length > 0 ) {
            // animation select
            animationsBody.clear();

            animationSelectBox = new Container.SelectBox(object.animations);
            animationsBody.add(animationSelectBox);
            animationsBody.add(startBtn);
            animationsBody.add(stopBtn);

            for (let i = 0; i < object.animations.length; i++) {
                let animation = object.animations[i];
                animations[ animation.name ] = mixer.clipAction( animation, object );
            }
        }
    } );

    events.objectRemoved.add( function ( object ) {

        if ( object !== null && object.animations.length > 0 ) {
            mixer.uncacheRoot( object );
        }

    } );
    return animationsContainer;
};

export default SideBar.Properties.Animations