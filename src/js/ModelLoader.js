/**
 * Created by front on 2019-02-21.
 */
import * as THREE from 'three.module'
import { FBXLoader } from "FBXLoader";
import { OBJLoader } from "OBJLoader";
import AddObjectCommand from './command/AddObjectCommand';

const ModelLoader = function (builder) {

    this.load = function (files) {
        let file = files[0];
        let filename = file.name;
        let extension = file.name.split('.').pop().toLowerCase();

        let fileReader = new FileReader();
        switch (extension) {
            case 'obj':
                fileReader.addEventListener('load', function (event) {
                    let objModel = event.target.result;

                    let object = new OBJLoader().parse(objModel);
                    object.name = '3d';

                    builder.execute( new AddObjectCommand ( object ))
                });
                fileReader.readAsText( files[0] );
                break;
            case 'fbx':
	            fileReader.addEventListener( 'load', function ( event ) {
		            let objModel = event.target.result;

		            let loader = new FBXLoader();
		            let object = loader.parse( objModel );
		            object.name = '3d';
		            var box = new THREE.Box3().setFromObject( object );
		            console.log( box.min, box.max, box.getSize() );
		            builder.execute( new AddObjectCommand( object ) );

	            }, false );
	            fileReader.readAsArrayBuffer( file );
	            break;
            default:
                console.error('it is not file for 3d object');
        }
    }
};

export default ModelLoader;