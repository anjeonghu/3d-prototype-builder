/**
 * Created by front on 2019-01-31.
 */

const Container = {};

Container.Ui = function (ele) {
    this.ele = ele || document.body;
};

Container.Ui.prototype = {
    add: function () {
        for (let i = 0; i < arguments.length; i ++ ) {
            let argument = arguments[i];

            if (argument instanceof  Container.Ui) {
                this.ele.appendChild(argument.ele);
            } else {
                console.log(argument.ele + '<- it not a Instance of Container.Ui')
            }
        }

        return this;
    },
    remove: function () {
        for (let i = 0; i < arguments.length; i++) {
            let argument = arguments[i];

            if (argument instanceof  Container.Ui) {
                this.ele.removeChild(argument.ele);
            } else {
                console.log(argument.ele + '<- it not a Instance of Container.Ui')
            }
        }

        return this;
    },
    innerElement: function () {
        this.ele.innerHTML = '';
        for(let i =0; i < arguments.length; i++) {
            if (arguments[i] instanceof  Container.Ui) {
                this.ele.appendChild(arguments[i].ele);

            } else {
                console.log(arguments[i].ele + '<- it not a Instance of Container.Ui')
            }
        }

        return this;
    },
    innerHTML: function (html) {
        this.ele.innerHTML = html;

        return this;
    },
    clear: function () {
        while( this.ele.children.length){
            this.ele.removeChild(this.ele.lastChild)
        }
        return this;
    },
    getValue: function () {
        return this.ele.value;
    },
    getAttribute: function (attr) {
        return this.ele.getAttribute(attr);
    },
    setAttribute: function (attr, value) {
        this.ele.setAttribute(attr, value);

        return this;
    },
    setClass: function ( name ) {
        this.ele.classList.add(name);

        return this;
    },
    removeAttribute: function (attr) {
        this.ele.removeAttribute(attr);
    },
    removeClass: function ( name ) {
        this.ele.classList.remove(name)
    },
    setId: function ( id ) {
        this.ele.id = id;

        return this;
    },
    setStyle: function (styles) {
        for ( let key in styles ) {
            this.ele.style[key] = styles[key];
        }

        return this;
    },
    setTextContent: function ( text ) {
        this.ele.textContent = text;

        return this;
    },
    focus: function () {
        this.ele.focus();

        return this;
    },
    onClick: function (callback) {
        this.ele.addEventListener('click', callback, false);
        return this;
    },
    onChange: function (callback) {
        this.ele.addEventListener('change', callback, false);
        return this;
    },
    onMouseDown: function (callback) {
        this.ele.addEventListener('pointerdown', callback, false);
        return this;
    },
    onMouseMove: function (callback) {
        this.ele.addEventListener('pointermove', callback, false);
        return this;
    },
    onKeyDown: function (callback) {
        this.ele.addEventListener('keypress', callback, false);
        return this;
    },
    onMouseUp: function (callback) {
        this.ele.addEventListener('pointerup', callback, false);
        return this;
    },
    onDragStart: function (callback) {
        this.ele.addEventListener('dragstart', callback, false);
        return this;
    },
    onDragOver: function (callback) {
        this.ele.addEventListener('dragover', callback, false);
        return this;
    },
    onDragEnter: function (callback) {
        this.ele.addEventListener('dragenter', callback, false);
        return this;
    },
    onDragLeave: function (callback) {
        this.ele.addEventListener('dragleave', callback, false);
        return this;
    },
    onDrop: function (callback) {
        this.ele.addEventListener('drop', callback, false);
        return this;
    },
    removeEventListener: function (type, callback) {
        this.ele.removeEventListener(type, callback, false);
        return this;
    }
};
Container.DIV = function () {
    let ele = document.createElement( 'div' );
    this.ele = ele;
    return this;
};
Container.DIV.prototype = Object.create(Container.Ui.prototype);
Container.DIV.constructor = Container.DIV;

Container.SPAN = function () {
    let ele = document.createElement( 'span' );
    this.ele = ele;
    return this;
};
Container.SPAN.prototype = Object.create(Container.Ui.prototype);
Container.SPAN.constructor = Container.SPAN;

Container.TEXT = function (text) {
    let ele = document.createElement( 'span' );
    ele.textContent = text;
    this.ele = ele;
    return this;
};
Container.TEXT.prototype = Object.create(Container.Ui.prototype);
Container.TEXT.constructor = Container.TEXT;

Container.Button = function (text) {
    Container.Ui.call(this);

    let button = document.createElement( 'button' );
    button.className = 'button';
    button.style.cursor = 'cursor';

    button.textContent = text;
    this.ele = button;

    return this;
};
Container.Button.prototype = Object.create(Container.Ui.prototype);
Container.Button.constructor = Container.Button;

Container.Label = function (name) {
    Container.Ui.call(this);

    let label = document.createElement('label');
    let text = name || '';

    let id = name.replace(/\s/gi, '').toLowerCase();

    label.textContent = text;

    this.ele = label;
    this.setClass('input-label');

    return this;
};
Container.Label.prototype = Object.create(Container.Ui.prototype);
Container.Label.prototype.constructor = Container.Label;

Container.Image = function (src) {
	Container.Ui.call(this);

	let ele = document.createElement('img');
	ele.src = src;

	this.ele = ele;
	this.setClass('image');
	return this;
};
Container.Image.prototype = Object.create(Container.Ui.prototype);
Container.Image.Constructor = Container.Image;

Container.Input = function (type, name) {
    Container.Ui.call(this);

    let label = document.createElement('label');
    let input = document.createElement('input');
    let text = name || '';

    let id = text.replace(/\s/gi, '').toLowerCase();

    input.type = type || 'text';
    input.id = id;

    input.addEventListener('keydown', function (event) {
        event.stopPropagation(); // webglview에서 keydown이벤트를 사용하기 때문에 이벤트 전파를 막기 위함.
    }, false);
    if (type === 'file') input.style.display = 'none';
    label.id = id;
    label.textContent = text;

    label.appendChild(input);

    this.ele = label;
    this.setClass('input-filed');
    this.setAttribute('step', 0.01);

    return this;
};
Container.Input.prototype = Object.create(Container.Ui.prototype);
Container.Input.prototype.constructor = Container.Input;
Container.Input.prototype.setValue = function (value) {
    let ele =  this.ele.childNodes.length > 1 ? this.ele.childNodes[1] : this.ele.childNodes[0];

    if (ele.type === 'number'){
        value = parseFloat(value).toFixed(2);
    }

    ele.value = value;

    return this;
};
Container.Input.prototype.getValue = function () {
    let ele =  this.ele.childNodes.length > 1 ? this.ele.childNodes[1] : this.ele.childNodes[0];
    let value = '';

    if (ele.type === 'file') {
        value = ele.files;
    } else {
        value = ele.value;
    }
    return value;
};
Container.Input.prototype.setPlaceHolder = function (value) {
    if (this.ele.childNodes.length > 1) {
        this.ele.childNodes[1].setAttribute('placeholder', value);
    } else {
        this.ele.childNodes[0].setAttribute('placeholder', value);
    }
    return this;
};
Container.Input.prototype.setAttribute = function (attr, value) {
    if (this.ele.childNodes.length > 1) {
        this.ele.childNodes[1].setAttribute(attr, value);
    } else {
        this.ele.childNodes[0].setAttribute(attr, value);
    }

    return this;
};
Container.Input.prototype.removeAttribute = function (attr) {
    if (this.ele.childNodes.length > 1) {
        this.ele.childNodes[1].removeAttribute(attr);
    } else {
        this.ele.childNodes[0].removeAttribute(attr);
    }
};
Container.Input.prototype.onChange = function (callback) {
    if (this.ele.childNodes.length > 1) {
        this.ele.childNodes[1].addEventListener('change', callback);
    } else {
        this.ele.childNodes[0].addEventListener('change', callback);
    }

    return this;
};

Container.List = function (items) {
    Container.Ui.call(this);

    let ul = document.createElement('ul');

    for (let i = 0; i < items.length; i ++) {
        let li = document.createElement('li');
        li.textContent = items[i];
        ul.appendChild(li);
    }

    this.ele = ul;

    return this;
};
Container.List.prototype = Object.create(Container.Ui.prototype);
Container.List.Constructor = Container.List;

Container.SelectBox = function (options) {
    Container.Ui.call(this);

    let selectBox = document.createElement('select');
    let selectBoxOptions = options;
    for (let i = 0; i < selectBoxOptions.length; i ++) {
        let option = selectBoxOptions[i];
        let ele = document.createElement('option');
        ele.value = option.name;
        ele.textContent = selectBoxOptions[i].name;
        selectBox.appendChild(ele);
    }

    this.ele = selectBox;
    this.setClass('select-box');
    return this;
};
Container.SelectBox.prototype = Object.create(Container.Ui.prototype);
Container.SelectBox.Constructor = Container.SelectBox;

Container.ImageBtn = function (src) {
    Container.Ui.call(this);

    let ele = document.createElement('span');
    let image = document.createElement('img');
    image.src = src;

    ele.appendChild(image);
    this.ele = ele;
    this.setClass('image-btn');
    this.setStyle({textAlign: 'center'});
    return this;
};
Container.ImageBtn.prototype = Object.create(Container.Ui.prototype);
Container.ImageBtn.Constructor = Container.ImageBtn;


Container.SideBar = function () {
    Container.Ui.call(this);

    let ele = document.createElement('div');
    this.ele = ele;
    this.setClass('sidebar');
    // this.setStyle({position: 'relative'});
    this.setStyle({position: 'absolute'});


    return this;
};

Container.SideBar.prototype = Object.create(Container.Ui.prototype);
Container.SideBar.prototype.constructor = Container.SideBar;


Container.WebGLView = function () {
    Container.Ui.call(this);

    let ele = document.createElement('div');
    this.ele = ele;
    this.setClass('webGl-view');
    this.setStyle({position: 'relative'});
    this.setAttribute('tabindex', 0)
    return this;
};

Container.WebGLView.prototype = Object.create(Container.Ui.prototype);
Container.WebGLView.prototype.constructor = Container.WebGLView;

Container.TabPanel = function () {
    Container.Ui.call(this);

    let ele = document.createElement('div');

    let style = {
        display: 'flex',
        height:  '31px',
        lineHeight: '31px',
        textAlign: 'center',
        borderBottom: '1px solid #38475f',
        paddingRight: '1px'
    };

    this.ele = ele;
    this.setClass('tab-category');
    this.setStyle(style);

    return this;
};

Container.TabPanel.prototype = Object.create(Container.Ui.prototype);
Container.TabPanel.prototype.constructor = Container.TabPanel;

Container.Tab = function (name) {
    Container.Ui.call(this);

    let ele = document.createElement('div');
    let text = name || 'No Name';

    let id = name.replace(/\s/gi, '');

    let style = {
        flex : '1 0 0%',
        cursor:  'pointer',
    };

    this.ele = ele;
    this.setClass('tab');
    this.setId(id);
    this.setStyle(style);
    this.setTextContent(text);

    return this;
};


Container.Tab.prototype = Object.create(Container.Ui.prototype);
Container.Tab.prototype.constructor = Container.Tab;


Container.Section = function (name) {
    Container.Ui.call(this);

    let ele = document.createElement('section');
    let header = document.createElement('div');
    let text = name || 'No Name';

    let id = name.replace(/\s/gi, '').toLowerCase();

    header.textContent = text;
    header.className = 'section-header';
    ele.appendChild(header);

    this.ele = ele;
    this.setId(id);
    this.setClass('section-' + id);

    return this;
};
Container.Section.prototype = Object.create(Container.Ui.prototype);
Container.Section.Constructor = Container.Section;

Container.SectionBody = function () {
    Container.Ui.call(this);

    let ele = document.createElement('div');

    ele.className = 'section-body';

    this.ele = ele;
    return this;
};
Container.SectionBody.prototype = Object.create(Container.Ui.prototype);
Container.SectionBody.Constructor = Container.SectionBody;

Container.Property = function (name) {
    Container.Ui.call(this);

    let ele = document.createElement('div');
    let header = document.createElement('div');
    let text = name || 'No Name';

    let id = name.replace(/\s/gi, '').toLowerCase();

    header.textContent = name;
    header.className = 'property-header';
    ele.appendChild(header);

    this.ele = ele;
    this.setId(id);
    this.setClass('property-' + id);

    return this;
};
Container.Property.prototype = Object.create(Container.Ui.prototype);
Container.Property.prototype.constructor = Container.Property;

Container.PropertyBody = function () {
    Container.Ui.call(this);

    let ele = document.createElement('div');

    ele.className = 'property-body';

    this.ele = ele;
    return this;
};
Container.PropertyBody.prototype = Object.create(Container.Ui.prototype);
Container.PropertyBody.prototype.constructor = Container.PropertyBody;

Container.DraggableContent = function (name) {
    Container.Ui.call(this);

    let ele = document.createElement('div');
    let text = name || 'No Name';

    let id = name.replace(/\s/gi, '');

    let style = {
        'display': 'flex',
        'flex-direction': 'row',
        'height': '75px',
        'width': '50%',
        'text-align': 'center',
        'justify-content': 'center',
        'align-items': 'center',
        'border-bottom': '1px solid #38475f',
        'border-right': '1px solid #38475f',
        'cursor': 'grab'
    };

    this.ele = ele;
    this.setClass('draggable');
    this.setId(id);
    this.setStyle(style);
    this.setAttribute('draggable', true);
    this.setTextContent(text);

    return this;
};
Container.DraggableContent.prototype = Object.create(Container.Ui.prototype);
Container.DraggableContent.prototype.constructor = Container.DraggableContent;

export default Container;