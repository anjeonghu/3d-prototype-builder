/**
 * Created by front on 2019-04-10.
 */

const colors = {
    black: {
        hex: '#000000',
        rgb: { r: 0, g: 0, b: 0}
    },
    white: {
        hex: '#ffffff',
        rgb: { r: 1, g: 1, b: 1}
    },
    red: {
        hex: '#FF0000',
        rgb: { r: 1, g: 0, b: 0}
    },
    green: {
        hex: '#00FF00',
        rgb: { r: 0, g: 1, b: 0}
    },
    blue: {
        hex: '#0000FF',
        rgb: { r: 0, g: 0, b: 1}
    },

};

export {colors}
