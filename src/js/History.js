/**
 * Created by front on 2019-02-15.
 */

import Command from './command/Command'

const History = function (builder) {
    this.list = [];

    Command.builder = builder;

    this.add = function (cmd) {
        this.list.push(cmd);
    };
    this.undo = function (cmd) {
        // do somthing
    };
    this.redo = function (cmd) {
        // do somthing
    }
};

export default History;
