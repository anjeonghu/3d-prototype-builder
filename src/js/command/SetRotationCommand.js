/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const SetRotationCommand = function (object, rotation) {
    Command.call(this);

    this.object = object;
    this.rotation = rotation;
};

SetRotationCommand.prototype = {
    execute: function () {
        this.object.rotation.copy(this.rotation);
        this.object.updateMatrixWorld(true);
        this.builder.history.add(this);
        this.builder.events.objectChanged.dispatch(this.object);
    },
    undo : function () {
        // do something
    }
};

export default SetRotationCommand;