/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const SetPositionCommand = function (object, position) {
    Command.call(this);

    this.object = object;
    this.position = position;
};

SetPositionCommand.prototype = {
    execute: function () {
        this.object.position.copy(this.position);
        this.object.updateMatrixWorld(true);
        this.builder.history.add(this);
        this.builder.events.objectChanged.dispatch(this.object);
    },
    undo : function () {
        // do something
    }
};

export default SetPositionCommand;