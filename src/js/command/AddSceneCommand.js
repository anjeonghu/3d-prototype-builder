/**
 * Created by front on 2019-02-07.
 */
import * as THREE from 'three.module'
import Command from './Command';

const AddSceneCommand = function (scene) {
    Command.call(this);

    this.scene = scene;
    this.scene.background = new THREE.Color( 0x152C40 );;
};

AddSceneCommand.prototype = {
    execute: function () {
        this.builder.setScene(this.scene);
        this.builder.select(null);
        this.builder.history.add(this);
        this.builder.events.sceneChanged.dispatch();
    },
    undo : function () {
        // do something
    }
};

export default AddSceneCommand