/**
 * Created by front on 2019-02-07.
 */
import * as THREE from 'three.module'
import Command from './Command'

const Add3DPointerCommand = function (object, parent) {
    Command.call(this);

    this.object = object;
    this.parent = parent;
    // let scale = new THREE.Vector3(1, 1, 1);
    // this.object.scale.copy(scale);

};

Add3DPointerCommand.prototype = {
    execute: function () {
        // let children = this.parent.children;
        // children.splice( children.length - 1, 0, this.object );
        // this.object.parent = this.parent;

        this.builder.add3DPointerObject(this.object, this.parent);
        this.builder.history.add(this);
    },
};

export default Add3DPointerCommand
