/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const RemoveObjectCommand = function (object) {
    Command.call(this);

    this.object = object;
};

RemoveObjectCommand.prototype = {
    execute: function () {
        this.builder.removeObject(this.object);
        this.builder.select(null);
    },
    undo : function () {
        // do something
    }
};

export default RemoveObjectCommand