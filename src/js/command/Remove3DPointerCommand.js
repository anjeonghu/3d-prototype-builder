/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const Remove3DPointerCommand = function (object) {
    Command.call(this);

    this.object = object;
};

Remove3DPointerCommand.prototype = {
    execute: function () {
        this.builder.remove3DPointerObject(this.object);
        this.builder.select(null);
    },
    undo : function () {
        // do something
    }
};

export default Remove3DPointerCommand