/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const SetScaleCommand = function (object, scale) {
    Command.call(this);

    this.object = object;
    this.scale = scale;
};

SetScaleCommand.prototype = {
    execute: function () {
        this.object.scale.copy(this.scale);
        this.object.updateMatrixWorld(true);
        this.builder.history.add(this);
        this.builder.events.objectChanged.dispatch(this.object);
    },
    undo : function () {
        // do something
    }
};

export default SetScaleCommand;