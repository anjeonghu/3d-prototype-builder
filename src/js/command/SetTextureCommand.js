/**
 * Created by front on 2019-02-07.
 */
import Command from './Command';

const SetTextureCommand = function (object, texture) {
    Command.call(this);

    this.object = object;
    this.object.width = Number(object.children[0].geometry.parameters.width);
    this.object.height = Number(object.children[0].geometry.parameters.height);
    this.oldTexture = object.map;
    this.newTexture = texture
};

SetTextureCommand.prototype = {
    execute: function () {
        this.object.children[0].material.map = this.newTexture;
        this.builder.history.add(this);
        this.builder.events.objectChanged.dispatch(this.object);
    },
    undo : function () {
        // do something
    }
};

export default SetTextureCommand;