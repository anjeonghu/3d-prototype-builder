import * as THREE from 'three.module'
import Command from './Command'

const AddObjectCommand = function (object, pos) {
    Command.call(this);

    if (object.name !== '3d'){
        let object3D = new THREE.Object3D().add(object);
        object3D.name = object.name;
        object3D.width = Number(object.geometry.parameters.width);
        object3D.height = Number(object.geometry.parameters.height);
        object3D.userData = object.userData;
        object3D.position.copy(pos || new THREE.Vector3(0, 0, 0));
        this.object = object3D;
    } else {
        this.object = object;
        // let scale = new THREE.Vector3(1, 1, 1);
        // this.object.scale.copy(scale);
        this.object.position.copy(pos || new THREE.Vector3(0, 0, 0));
    }

};

AddObjectCommand.prototype = {
    execute: function () {
        if (this.builder.rayMode === 'pointer') {
            this.builder.events.pointerModeEnded.dispatch();
        }
        this.builder.addObject(this.object);
        this.builder.select(this.object);
        this.builder.history.add(this);
    },
    undo : function () {
        // do something
    }
};

export default AddObjectCommand