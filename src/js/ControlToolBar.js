import * as THREE from 'three.module'
import Container from './Container'

const ControlToolBar = function (builder) {
    let events = builder.events;
    let container = new Container.DIV();
    let transLateImg = new Container.Image('static/asset/icons/translate.svg');
    let rotationImg = new Container.Image('static/asset/icons/rotate.svg');
    let scaleImg = new Container.Image('static/asset/icons/scale.svg');
    let transLateControlBtn = new Container.Button().add(transLateImg).setAttribute('disabled', 'disabled');
    let rotationControlBtn = new Container.Button().add(rotationImg).setAttribute('disabled', 'disabled');
    let scaleControlBtn = new Container.Button().add(scaleImg).setAttribute('disabled', 'disabled');
    let toggleCameraBtn = new Container.Button('2D');
    let camera2DMode = false; // true면 2D false면 3D

    container.setClass('control-toolbar');

    transLateControlBtn.onClick(function (){
        removeAllSelected();
        transLateControlBtn.setClass('selected');
        events.transformModeChanged.dispatch('translate');
    });
    rotationControlBtn.onClick(function () {
        removeAllSelected();
        rotationControlBtn.setClass('selected');
        events.transformModeChanged.dispatch('rotate');
    });
    scaleControlBtn.onClick(function () {
        removeAllSelected();
        scaleControlBtn.setClass('selected');
        events.transformModeChanged.dispatch('scale');
    });
    toggleCameraBtn.onClick(function () {
        removeAllSelected();
        setAllDiabled();
        camera2DMode = !camera2DMode;
        if (camera2DMode) {
            toggleCameraBtn.setClass('selected');
            let camera = builder.ORTHO_CAMERA;
            events.cameraChanged.dispatch(camera);
        } else {
            let camera = builder.DEFAULT_CAMERA;
            toggleCameraBtn.removeClass('selected');
            events.cameraChanged.dispatch(camera);
        }

    });


    function removeAllSelected () {
        transLateControlBtn.removeClass('selected');
        rotationControlBtn.removeClass('selected');
        scaleControlBtn.removeClass('selected');
    }
    function removeAllDiabled () {
        transLateControlBtn.removeAttribute('disabled');
        rotationControlBtn.removeAttribute('disabled');
        scaleControlBtn.removeAttribute('disabled');
    }
    function setAllDiabled () {
        transLateControlBtn.setAttribute('disabled', 'disabled');
        rotationControlBtn.setAttribute('disabled', 'disabled');
        scaleControlBtn.setAttribute('disabled', 'disabled');
    }

    container.add(transLateControlBtn);
    container.add(rotationControlBtn);
    container.add(scaleControlBtn);
    container.add(toggleCameraBtn);

    events.objectSelected.add(function (object) {
        if (object === null || object.name === 'Target') {
            removeAllSelected();
            setAllDiabled()
        } else {
            removeAllSelected();
            removeAllDiabled();
            transLateControlBtn.setClass('selected');

            let camera = builder.camera;
            if (camera instanceof THREE.OrthographicCamera) {
                rotationControlBtn.setAttribute('disabled', 'disabled');
            } else if (camera instanceof THREE.PerspectiveCamera){
                rotationControlBtn.removeAttribute('disabled', 'disabled');
            }
            events.transformModeChanged.dispatch('translate');
        }
    });

    return container;
};

export default ControlToolBar;