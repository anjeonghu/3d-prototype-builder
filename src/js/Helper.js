/**
 * Created by front on 2019-01-29.
 */


const Helper = function () {
    this.PI = Math.PI;
};

Helper.prototype = {
    getRadianToDegree: function  (radian) {
        // radian 각도 degree 각도로
        return radian / (this.PI / 180);
    },
    getDegreeToRadian: function (deg) {
        // degree 각도 radian 각도로
        return (this.PI / 180) * deg;
    },
    getMagnitude: function (vec) {
        // 벡터의 크기 계산
        return Math.sqrt(Math.pow(vec.x,2)+Math.pow(vec.y,2)+Math.pow(vec.z,2))
    },
    normalizeVector: function (vec) {
        // 벡터의 정규화
        let mag = this.getMagnitude(vec);
        return { x:vec.x / mag, y:vec.y / mag, z:vec.z / mag}
    },
    getScaleMatrix: function (scale) {
        let s = new THREE.Matrix4();
        s.identity();
        s.scale(scale);
        return s;
    },
    getRotateMatrix: function (euler) {
        let r = new THREE.Matrix4();
        r.identity();
        r.makeRotationFromEuler(euler);
        return r;
    },
    getTransLateMatrix: function (vector) {
        let t = new THREE.Matrix4();
        t.identity();
        t.setPosition(vector);
        return t;
    }
};

export default Helper;