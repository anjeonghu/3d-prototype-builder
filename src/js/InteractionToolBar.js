import * as THREE from 'three.module'
import Container from './Container'

const InteractionToolBar = function (builder) {
    let events = builder.events;
    let selectedObject = builder.selectedObject;
    let container = new Container.DIV();
    let togglePointerBtn = new Container.ImageBtn('static/asset/icons/clipboard_check_icon.svg')
        .onClick(pointerSelected);
    let toggleAnimationBtn = new Container.ImageBtn('static/asset/icons/animation_icon_2x.png')
        .setClass('disabled')
        .onClick(animationSelected);
    let pSelected = false
        ,aSelected = false;
    container.setClass('interaction-toolbar');

    container.add(togglePointerBtn);
    container.add(toggleAnimationBtn);

    events.pointerModeStarted.add( function () {
    });

    events.pointerModeEnded.add( function () {
        togglePointerBtn.removeClass('selected');
        togglePointerBtn.setClass('disabled');
    });

    events.objectSelected.add(function () {
        selectedObject = builder.selectedObject;
        if (selectedObject) {
            toggleAnimationBtn.removeClass('disabled');
        } else {
            toggleAnimationBtn.setClass('disabled');
        }
    });

    function pointerSelected (event) {
        event.preventDefault();

        pSelected = !pSelected;
        aSelected = false;
        if(pSelected) {
            toggleAnimationBtn.removeClass('selected');
            toggleAnimationBtn.setClass('disabled');

            togglePointerBtn.setClass('selected');

            events.pointerModeStarted.dispatch();
        } else {
            togglePointerBtn.removeClass('selected');

            events.pointerModeEnded.dispatch();
        }
    }

    function animationSelected (event) {
        event.preventDefault();
        if(!selectedObject)  return;
        aSelected = !aSelected;
        pSelected = false;
        if(aSelected) {
            togglePointerBtn.removeClass('selected');
            toggleAnimationBtn.setClass('selected');

            events.setAnimationModeStarted.dispatch();
        } else {
            toggleAnimationBtn.removeClass('selected');

            events.setAnimationModeEnded.dispatch();
        }
    }

    return container;
};

export default InteractionToolBar;