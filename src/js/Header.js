
import Container from './Container'
import axios from 'axios'

const Header = function (builder) {
    let events = builder.events;
    let helper = builder.helper;
    let container = new Container.DIV();
    let title = new Container.TEXT('MAXST').setClass('title');
    let saveBtn = new Container.Button('SAVE').setClass('btn-neon');//.setAttribute('disabled', 'disabled');

    container.setClass('header');


    saveBtn.onClick(function (){
        let objects = builder.objects;
        let target = {};
        let elements = [];
        objects.forEach((object, index) => {
            if (object.name === 'Target') {
                target.uuid = object.uuid;
                target.type = 'Target';
                target.srcUrl = 'https://raw.githubusercontent.com/happenask/staticStorage/master/images/Robot.jpg';
                target.realWidth = object.width;
                target.realHeight = object.height;
                target.position = object.position;
                target.rotation = {x: helper.getRadianToDegree(object.rotation.x) , y: helper.getRadianToDegree(object.rotation.y), z: helper.getRadianToDegree(object.rotation.z)};
                target.scale = object.scale;
            } else {
                let element = {};
                element.uuid = object.uuid;
                element.type = object.name;
                element.width = object.width;
                element.height = object.height;
                element.position = object.position;
                element.rotation = {x: helper.getRadianToDegree(object.rotation.x), y: helper.getRadianToDegree(object.rotation.y), z: helper.getRadianToDegree(object.rotation.z)};
                element.scale = object.scale;
                element.content = object.userData;
                element.userData = null;
                element.child = [];

                elements.push(element);
            }
        });

        const req = {target, elements};
        console.log(req);

        axios.post('http://localhost:3000/api', req)
            .then((res) => {
                console.log(res)
            })
            .catch((error) => {
                console.log(error);
                alert(error);
            });
    });

    container.add(title);
    container.add(saveBtn);

    return container;
};

export default Header;