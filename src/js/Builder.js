/**
 * Created by front on 2019-01-28.
 */
import * as THREE from 'three.module'
import Helper from './Helper';
import History from './History';
import ModelLoader from './ModelLoader';


const BUilder = function () {
    this.width = 1000;
    this.height = 1000;

    this.DEFAULT_CAMERA = new THREE.PerspectiveCamera(
        60,
        this.width / this.height,
        0.01,
        1000
    );
    this.DEFAULT_CAMERA.name = 'PerspectiveCamera';
    this.DEFAULT_CAMERA.position.set( 0, 0.3, 10);
    // this.DEFAULT_CAMERA.position.set( 0, 200, 300 );
    this.DEFAULT_CAMERA.lookAt( new THREE.Vector3() );

    this.ORTHO_CAMERA = new THREE.OrthographicCamera(
        this.width / - 2,
        this.width / 2,
        this.height / 2,
        this.height / - 2,
        0.1,
        10
    );
    this.ORTHO_CAMERA.name = 'OrthographicCamera';
    this.ORTHO_CAMERA.position.set( 0, 0.5, 0 );
    this.ORTHO_CAMERA.lookAt( new THREE.Vector3() );

    this.helper = new Helper();
    this.target = {};
    this.loader = new ModelLoader(this);
    this.light = new THREE.DirectionalLight(0xffffff, 0.8);
    this.scenes = {};
    this.scene = new THREE.Scene();
    this.scene.name = 'Scene #1';
    this.scenes[this.scene.uuid] = this.scene;
    // this.scene.background = new THREE.Color( 0xF6F6F6 );
    this.scene.background = new THREE.Color( 0x152C40 );
    this.renderer = new THREE.WebGLRenderer( { antialias: true } );
    this.camera = this.DEFAULT_CAMERA.clone();
    this.mixer = this.mixer = new THREE.AnimationMixer( this.scene );
    this.objects = [];
    this.geometries = {};
    this.materials = {};
    this.pointers = {};
    this.pointerInfos = {};
    this.rayMode = 'init'; //mode: 'init', 'pointer'
    this.history = new History (this);
    this.selectedObject = null;
    this.draggingObject = null;

    let Signal = signals.Signal;

    this.events = {
        builderCleared: new Signal(),
        cameraChanged: new Signal(),
        helperAdded: new Signal(),
        helperRemoved: new Signal(),
        objectAdded: new Signal(),
        objectChanged: new Signal(),
        objectRemoved: new Signal(),
        objectSelected: new Signal(),
        rendererChanged: new Signal(),
        renderStart: new Signal(),
        sceneAdded: new Signal(),
        sceneChanged: new Signal(),
        transformModeChanged: new Signal(),
        updateTransformInput: new Signal(),
        animationStarted: new Signal(),
        animationStopped: new Signal(),
        setAnimationModeStarted: new Signal(),
        setAnimationModeEnded: new Signal(),
        addAnimationTranslate: new Signal(),
        addAnimationScale: new Signal(),
        addAnimationRotation: new Signal(),
        removeAnimationTranslate: new Signal(),
        removeAnimationScale: new Signal(),
        removeAnimationRotation: new Signal(),
        pointerModeStarted: new Signal(),
        pointerModeEnded: new Signal(),
        windowResize: new Signal()
    }
};


BUilder.prototype =  {
    setScene: function (scene) {
        if(scene !== null && this.scene !== scene ) {
            if (scene.name === '') scene.name = `Scene #${Object.keys(this.scenes).length + 1}`;
            this.scene = scene;
            this.scenes[scene.uuid] = this.scenes[scene.uuid] || scene;
        }

        return this;
    },
    setCamara: function (camera){
        this.camera = camera;
        if ( this.camera && this.camera instanceof THREE.PerspectiveCamera) {

            this.camera.aspect = this.width / this.height;

        } else if (this.camera instanceof THREE.OrthographicCamera) {
            this.camera.left = -this.width / 2000;
            this.camera.right = this.width / 2000;
            this.camera.top = this.height / 2000;
            this.camera.bottom = -this.height / 2000;
    }
        this.camera.updateProjectionMatrix();

        return this;
    },
    setSize: function (width, height) {
        this.width = width;
        this.height = height;

        if ( this.camera && this.camera instanceof THREE.PerspectiveCamera) {

            this.camera.aspect = this.width / this.height;

            this.camera.updateProjectionMatrix();
        } else if (this.camera && this.camera instanceof THREE.OrthographicCamera) {
            this.camera.left = -this.width / 2000;
            this.camera.right = this.width / 2000;
            this.camera.top = this.height / 2000;
            this.camera.bottom = -this.height / 2000;

            this.camera.updateProjectionMatrix();
        }


        if ( this.renderer ) {

            this.renderer.setSize( width, height );

        }
        return this;
    },
    execute: function (cmd) {
        cmd.execute();
    },
    select: function (object) {
        if (this.selectedObject === object) return;
        this.selectedObject = object || null;

        this.events.objectSelected.dispatch(this.selectedObject);
    },
    getObject: function (uuid) {
        return this.objects.find(object => object.uuid === uuid);
    },
    addObject: function ( object, parent, index ) {
        object.traverse( ( child ) => {

            if ( child.geometry !== undefined ) this.addGeometry( child.geometry );
            if ( child.material !== undefined ) this.addMaterial( child.material );

        } );
        object.castShadow = true;
        object.receiveShadow = false;

	    // if ( parent === undefined ) {
	    //
		//     this.scene.add( object );
	    //
	    // } else {
	    //
		//     parent.children.splice( index, 0, object );
		//     object.parent = parent;
	    //
	    // }

        this.scene.add( object );
        this.objects.push( object );

        this.events.objectAdded.dispatch( object );
    },
    removeObject: function ( object ) {
        if (object.parent === null) return;

        object.parent.remove( object );
        this.objects.splice(this.objects.indexOf(object), 1);

        if (this.pointers[object.uuid]) {
            let pointers = this.pointers[object.uuid];
            for (let i = 0; i < pointers.length; i++) {
                let pointer = pointers[i];
                this.objects.splice(this.objects.indexOf(pointer), 1);

                pointer.parent.remove( pointer );
            }
            delete this.pointers[object.uuid];
        }
        this.events.objectRemoved.dispatch( object );
    },
    remove3DPointerObject: function ( object ) {
        if (object.parent === null) return;

        object.parent.remove( object );

        let uuid = object.uuid;
        let puuid = this.pointerInfos[uuid].puuid;

        this.pointers[ puuid ] = this.pointers[ puuid ].filter((pointer) => {
           return  pointer.uuid !== uuid;
        });
        delete this.pointerInfos[uuid];
        this.events.objectRemoved.dispatch( object );
    },
    addGeometry: function ( geometry ) {
        this.geometries[ geometry.uuid ] = geometry;
    },
    addMaterial: function ( material ) {
        this.materials[ material.uuid ] = material;
    },
    add3DPointerObject: function ( pointer, parent ) {
        this.objects.push( pointer );
	    this.scene.add( pointer );
        if (!this.pointers[ parent.uuid ]) {
            this.pointers[ parent.uuid ] = [];
        }
        this.pointers[ parent.uuid ].push(pointer);
        this.pointerInfos[ pointer.uuid ]= {
            puuid: parent.uuid,
            object: pointer
        };
        this.events.objectAdded.dispatch( pointer );
    },
};

export default BUilder;
